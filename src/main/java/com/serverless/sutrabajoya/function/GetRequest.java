package com.serverless.sutrabajoya.function;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.serverless.sutrabajoya.model.Request;
import com.serverless.sutrabajoya.model.ServerlessInput;
import com.serverless.sutrabajoya.model.ServerlessOutput;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

public class GetRequest implements RequestHandler<ServerlessInput, ServerlessOutput> {
	// DynamoDB table name for storing client metadata.
	private static final String REQUEST_TABLE_NAME = System.getenv("REQUEST_TABLE_NAME");

	@Override
	public ServerlessOutput handleRequest(ServerlessInput serverlessInput, Context context) {

		ServerlessOutput output = new ServerlessOutput();

		String token = serverlessInput.getHeaders().get("token");
		Claims claims = null;
		if (null != token) {
			// This line will throw an exception if it is not a signed JWS (as expected)
			try {
				claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(System.getenv("KEY_SECRET")))
						.parseClaimsJws(token).getBody();

				// OK, we can trust this JWT
			} catch (SignatureException e) {
				// don't trust the JWT!
				output.setStatusCode(401);
				return output;
			}
		} else {

			output.setStatusCode(401);
			return output;
		}

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Access-Control-Allow-Origin", "*");
		headers.put("content-type", "application/json");
		output.setHeaders(headers);
		AmazonDynamoDB amazonDynamoDb = AmazonDynamoDBClientBuilder.standard().build();

		Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();

		expressionAttributeValues.put(":email", new AttributeValue().withS(claims.get("email").toString()));

		ScanRequest scanRequest = new ScanRequest().withTableName(REQUEST_TABLE_NAME)
				.withFilterExpression("userEmail = :email").withExpressionAttributeValues(expressionAttributeValues);
		ObjectMapper mapper = new ObjectMapper();
		ScanResult result = amazonDynamoDb.scan(scanRequest);
		List<Request> itemList = new ArrayList<>();
		Request request = null;
		for (Map<String, AttributeValue> item : result.getItems()) {
			request = new Request();
			request.setId(item.get("id").getS());
			request.setServiceId(item.get("serviceId").getS());
			request.setDescription(item.get("description").getS());
			request.setUserEmail(item.get("userEmail").getS());
			request.setStartDate(new Date(Long.parseLong(item.get("startDate").getS())));
			request.setEndDate(new Date(Long.parseLong(item.get("endDate").getS())));
			request.setCreationDate(new Date(Long.parseLong(item.get("creationDate").getS())));
			itemList.add(request);
		}
		output.setStatusCode(200);

		try {
			output.setBody(mapper.writeValueAsString(itemList));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}
}
