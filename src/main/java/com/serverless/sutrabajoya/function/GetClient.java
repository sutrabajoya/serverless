package com.serverless.sutrabajoya.function;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.serverless.sutrabajoya.model.Client;
import com.serverless.sutrabajoya.model.ServerlessInput;
import com.serverless.sutrabajoya.model.ServerlessOutput;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

import javax.xml.bind.DatatypeConverter;

public class GetClient implements RequestHandler<ServerlessInput, ServerlessOutput> {
	// DynamoDB table name for storing client metadata.
	private static final String CLIENT_TABLE_NAME = System.getenv("CLIENT_TABLE_NAME");

	@Override
	public ServerlessOutput handleRequest(ServerlessInput serverlessInput, Context context) {

		ServerlessOutput output = new ServerlessOutput();

		String token = serverlessInput.getHeaders().get("token");
		Claims claims = null;
		if (null != token) {
			// This line will throw an exception if it is not a signed JWS (as expected)
			try {
				claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(System.getenv("KEY_SECRET")))
						.parseClaimsJws(token).getBody();

				// OK, we can trust this JWT
			} catch (SignatureException e) {
				// don't trust the JWT!
				output.setStatusCode(401);
				return output;
			}
		} else {

			output.setStatusCode(401);
			return output;
		}

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Access-Control-Allow-Origin", "*");
		headers.put("content-type", "application/json");
		output.setHeaders(headers);
		AmazonDynamoDB amazonDynamoDb = AmazonDynamoDBClientBuilder.standard().build();
		DynamoDB dynamoDB = new DynamoDB(amazonDynamoDb);

		Table table = dynamoDB.getTable(CLIENT_TABLE_NAME);

		Item item = null;

		ObjectMapper mapper = new ObjectMapper();
		QuerySpec spec = new QuerySpec().withKeyConditionExpression("email = :v_email")
				.withValueMap(new ValueMap().withString(":v_email", claims.get("email").toString()));

		ItemCollection<QueryOutcome> items = table.query(spec);
		Iterator<Item> iterator = items.iterator();

		if (iterator.hasNext()) {
			item = iterator.next();
			try {
			Client client = mapper.readValue(item.toJSON(), Client.class);
			client.setPassword("");
			output.setStatusCode(200);
			
				output.setBody(mapper.writeValueAsString(client));
			} catch (JsonProcessingException e) {
				output.setStatusCode(400);
				output.setBody("{\"message\":\"Faltan parámetros\"}");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return output;
	}
}
