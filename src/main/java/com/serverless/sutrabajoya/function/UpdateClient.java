package com.serverless.sutrabajoya.function;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.jasypt.util.password.StrongPasswordEncryptor;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.serverless.sutrabajoya.model.Client;
import com.serverless.sutrabajoya.model.ServerlessInput;
import com.serverless.sutrabajoya.model.ServerlessOutput;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

/**
 * Lambda function that triggered by the API Gateway event "POST /". It reads
 * all the query parameters as the metadata for this article and stores them to
 * a DynamoDB table. It reads the payload as the content of the article and
 * stores it to a S3 bucket.
 */
public class UpdateClient implements RequestHandler<ServerlessInput, ServerlessOutput> {

	// DynamoDB table name for storing client metadata.
	private static final String CLIENT_TABLE_NAME = System.getenv("CLIENT_TABLE_NAME");

	@Override
	public ServerlessOutput handleRequest(ServerlessInput serverlessInput, Context context) {
		ServerlessOutput output = new ServerlessOutput();
		String token = serverlessInput.getHeaders().get("token");
		Claims claims = null;
		if (null != token) {
			// This line will throw an exception if it is not a signed JWS (as expected)
			try {
				claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(System.getenv("KEY_SECRET")))
						.parseClaimsJws(token).getBody();

				// OK, we can trust this JWT
			} catch (SignatureException e) {
				// don't trust the JWT!
				output.setStatusCode(401);
				return output;
			}
		} else {

			output.setStatusCode(401);
			return output;
		}
		AmazonDynamoDB amazonDynamoDb = AmazonDynamoDBClientBuilder.standard().build();

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Access-Control-Allow-Origin", "*");
		headers.put("content-type", "application/json");
		output.setHeaders(headers);
		ObjectMapper mapper = new ObjectMapper();
		Client client = null;
		try {
			client = mapper.readValue(serverlessInput.getBody(), Client.class);
		} catch (JsonParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (null == client) {
			output.setStatusCode(400);
			output.setBody("{\"message\":\"Faltan parámetros\"}");
		} else {
			try {
				StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
				String encryptedPassword = passwordEncryptor.encryptPassword(client.getPassword());
				Map<String, AttributeValue> attributes = new HashMap<>();
				attributes.put("identification", new AttributeValue().withS(client.getIdentification()));
				attributes.put("name", new AttributeValue().withS(client.getName()));
				attributes.put("lastname", new AttributeValue().withS(client.getLastname()));
				attributes.put("email", new AttributeValue().withS(client.getEmail()));
				attributes.put("password", new AttributeValue().withS(encryptedPassword));
				attributes.put("address", new AttributeValue().withS(client.getAddress()));
				attributes.put("city", new AttributeValue().withS(client.getCity()));
				attributes.put("cellphone", new AttributeValue().withS(client.getCellphone()));
				attributes.put("phone", new AttributeValue().withS(client.getPhone()));
				attributes.put("resume", new AttributeValue().withS(client.getResume()));
				if (!claims.getId().equals(client.getEmail())) {
					output.setStatusCode(401);
					return output;
				}
				amazonDynamoDb.putItem(new PutItemRequest().withTableName(CLIENT_TABLE_NAME).withItem(attributes));
				client.setPassword(encryptedPassword);
				output.setStatusCode(200);
				output.setBody(mapper.writeValueAsString(client));
			} catch (JsonProcessingException e) {
				output.setStatusCode(400);
				output.setBody("{\"message\":\"Faltan parámetros\"}");
			} catch (Exception e) {
				output.setStatusCode(500);
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				output.setBody(sw.toString());
			}

		}
		return output;
	}
}