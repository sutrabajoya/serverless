package com.serverless.sutrabajoya.function;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.xml.bind.DatatypeConverter;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.serverless.sutrabajoya.model.Estimation;
import com.serverless.sutrabajoya.model.ServerlessInput;
import com.serverless.sutrabajoya.model.ServerlessOutput;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

public class CreateEstimation implements RequestHandler<ServerlessInput, ServerlessOutput> {
	// DynamoDB table name for storing client metadata.
	private static final String ESTIMATION_TABLE_NAME = System.getenv("ESTIMATION_TABLE_NAME");

	@Override
	public ServerlessOutput handleRequest(ServerlessInput serverlessInput, Context context) {
		// Using builder to create the clients could allow us to dynamically load the
		// region from the AWS_REGION environment
		// variable. Therefore we can deploy the Lambda functions to different regions
		// without code change.
		ServerlessOutput output = new ServerlessOutput();
		String token = serverlessInput.getHeaders().get("token");
		Claims claims = null;
		if (null != token) {
			// This line will throw an exception if it is not a signed JWS (as expected)
			try {
				claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(System.getenv("KEY_SECRET")))
						.parseClaimsJws(token).getBody();

				// OK, we can trust this JWT
			} catch (SignatureException e) {
				// don't trust the JWT!
				output.setStatusCode(401);
				return output;
			}
		} else {

			output.setStatusCode(401);
			return output;
		}
		AmazonDynamoDB amazonDynamoDb = AmazonDynamoDBClientBuilder.standard().build();
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Access-Control-Allow-Origin", "*");
		headers.put("content-type", "application/json");
		output.setHeaders(headers);
		ObjectMapper mapper = new ObjectMapper();
		String json = serverlessInput.getBody();
		Estimation estimation = null;
		try {
			estimation = mapper.readValue(json, Estimation.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		if (null == estimation) {
			output.setStatusCode(400);
			output.setBody("{\"message\":\"Faltan parámetros\"}");
		} else {

			try {
				Map<String, AttributeValue> attributes = new HashMap<>();
				attributes.put("id", new AttributeValue().withS(UUID.randomUUID().toString()));
				attributes.put("userEmail", new AttributeValue().withS(claims.get("email").toString()));
				attributes.put("requestId", new AttributeValue().withS(estimation.getRequestId()));
				attributes.put("description", new AttributeValue().withS(estimation.getDescription()));
				attributes.put("startDate",
						new AttributeValue().withS(String.valueOf(estimation.getStartDate().getTime())));
				attributes.put("cost", new AttributeValue().withS(estimation.getCost()));
				attributes.put("validDays", new AttributeValue().withS(estimation.getValidDays()));
				attributes.put("creationDate", new AttributeValue().withS(String.valueOf(new Date().getTime())));
				amazonDynamoDb.putItem(new PutItemRequest().withTableName(ESTIMATION_TABLE_NAME).withItem(attributes));

				output.setStatusCode(200);
				// output.setBody(gson.toJson(client));
			} catch (Exception e) {
				output.setStatusCode(500);
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				output.setBody("{\"message\":\"Internal server error\"}");
				// output.setBody(sw.toString());
			}
		}
		return output;
	}

}
