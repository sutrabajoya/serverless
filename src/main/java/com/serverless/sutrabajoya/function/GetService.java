package com.serverless.sutrabajoya.function;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.serverless.sutrabajoya.model.ServerlessInput;
import com.serverless.sutrabajoya.model.ServerlessOutput;
import com.serverless.sutrabajoya.model.Service;

public class GetService implements RequestHandler<ServerlessInput, ServerlessOutput> {
	// DynamoDB table name for storing client metadata.
	private static final String SERVICE_TABLE_NAME = System.getenv("SERVICE_TABLE_NAME");

	@Override
	public ServerlessOutput handleRequest(ServerlessInput serverlessInput, Context context) {

		ServerlessOutput output = new ServerlessOutput();

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Access-Control-Allow-Origin", "*");
		headers.put("content-type", "application/json");
		output.setHeaders(headers);
		AmazonDynamoDB amazonDynamoDb = AmazonDynamoDBClientBuilder.standard().build();
		ScanRequest scanRequest;
		String searchString = null;
		String serviceId = null;
		if (null != serverlessInput.getQueryStringParameters()) {
			searchString = serverlessInput.getQueryStringParameters().get("search");
			serviceId = serverlessInput.getQueryStringParameters().get("id");
		}
		
		ObjectMapper mapper = new ObjectMapper();

		if(null!=serviceId && !"".equals(serviceId)) {
			DynamoDB dynamoDB = new DynamoDB(amazonDynamoDb);
			Table table = dynamoDB.getTable(SERVICE_TABLE_NAME);

			Item item = table.getItem("id", serviceId);
			output.setStatusCode(200);
			output.setBody(item.toJSON());
			return output;
		}
		
		if (null != searchString && !"".equals(searchString)) {

			Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();

			expressionAttributeValues.put(":string", new AttributeValue().withS(searchString));

			scanRequest = new ScanRequest().withTableName(SERVICE_TABLE_NAME)
					.withFilterExpression("contains(serviceName, :string) or contains(description, :string)")
					.withExpressionAttributeValues(expressionAttributeValues);

		} else {

			scanRequest = new ScanRequest().withTableName(SERVICE_TABLE_NAME);
		}

		ScanResult result = amazonDynamoDb.scan(scanRequest);
		List<Service> itemList = new ArrayList<>();
		Service service = null;
		for (Map<String, AttributeValue> item : result.getItems()) {
			service = new Service();
			service.setId(item.get("id").getS());
			service.setServiceName(item.get("serviceName").getS());
			service.setDescription(item.get("description").getS());
			service.setUserEmail(item.get("userEmail").getS());
			service.setUserName(item.get("userName").getS());
			service.setCost(item.get("cost").getS());
			itemList.add(service);
		}

		output.setStatusCode(200);

		try {
			output.setBody(mapper.writeValueAsString(itemList));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}
}
