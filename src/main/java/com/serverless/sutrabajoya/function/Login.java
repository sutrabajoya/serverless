package com.serverless.sutrabajoya.function;

import java.io.IOException;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.jasypt.util.password.StrongPasswordEncryptor;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.serverless.sutrabajoya.model.Client;
import com.serverless.sutrabajoya.model.ServerlessInput;
import com.serverless.sutrabajoya.model.ServerlessOutput;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class Login implements RequestHandler<ServerlessInput, ServerlessOutput> {

	private static final String CLIENT_TABLE_NAME = System.getenv("CLIENT_TABLE_NAME");

	@Override
	public ServerlessOutput handleRequest(ServerlessInput serverlessInput, Context context) {
		ServerlessOutput output = new ServerlessOutput();
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Access-Control-Allow-Origin", "*");
		headers.put("content-type", "application/json");
		output.setHeaders(headers);
		AmazonDynamoDB amazonDynamoDb = AmazonDynamoDBClientBuilder.standard().build();
		DynamoDB dynamoDB = new DynamoDB(amazonDynamoDb);

		Table table = dynamoDB.getTable(CLIENT_TABLE_NAME);

		Item item = null;
		ObjectMapper mapper = new ObjectMapper();
		Client clientRequest = null;
		if (null != serverlessInput.getBody()) {

			try {
				clientRequest = mapper.readValue(serverlessInput.getBody(), Client.class);

				QuerySpec spec = new QuerySpec().withKeyConditionExpression("email = :v_email")
						.withValueMap(new ValueMap().withString(":v_email", clientRequest.getEmail()));

				ItemCollection<QueryOutcome> items = table.query(spec);
				Iterator<Item> iterator = items.iterator();

				if (iterator.hasNext()) {
					item = iterator.next();

					Client client = mapper.readValue(item.toJSON(), Client.class);
					StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
					if (passwordEncryptor.checkPassword(clientRequest.getPassword(), client.getPassword())) {
						headers.put("content-type", "text/plain");
						output.setStatusCode(200);
						output.setBody(createJWT(client, "sutrabajoya", "subject", 2629746000L));
						return output;
					}
				}
			} catch (JsonParseException e) {
				output.setStatusCode(400);
				output.setBody("{\"message\":\"Faltan parámetros\"}");
				return output;
			} catch (JsonMappingException e) {
				output.setStatusCode(400);
				output.setBody("{\"message\":\"Faltan parámetros\"}");
				return output;
			} catch (IOException e) {
				output.setStatusCode(400);
				output.setBody("{\"message\":\"Faltan parámetros\"}");
				return output;
			}
		}
		output.setStatusCode(400);
		output.setBody("{\"message\":\"El cliente no existe\"}");

		return output;
	}

	// Sample method to construct a JWT
	private String createJWT(Client client, String issuer, String subject, long ttlMillis) {

		// The JWT signature algorithm we will be using to sign the token
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);
		Map<String, Object> claims = new HashMap<String, Object>();
		claims.put("email", client.getEmail());
		claims.put("name", client.getName());
		// We will sign our JWT with our ApiKey secret
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(System.getenv("KEY_SECRET"));
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

		// Let's set the JWT Claims
		JwtBuilder builder = Jwts.builder().setClaims(claims).setIssuedAt(now).setSubject(subject).setIssuer(issuer)
				.signWith(signatureAlgorithm, signingKey);

		// if it has been specified, let's add the expiration
		if (ttlMillis >= 0) {
			long expMillis = nowMillis + ttlMillis;
			Date exp = new Date(expMillis);
			builder.setExpiration(exp);
		}

		// Builds the JWT and serializes it to a compact, URL-safe string
		return builder.compact();
	}
}
